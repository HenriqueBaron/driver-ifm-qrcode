﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Modbus;
using NetFwTypeLib;

namespace Auttom.OC2018.DriverIFMO2I
{
    public partial class Service1 : ServiceBase
    {
        IPAddress ipLeitor;
        int portaLeitor;

        TcpClient clientLeitor;
        ModbusSlaveTCP modbusServer;
        bool modbusServerAberto = false;
        bool pararServico = false;

        public Service1()
        {
            InitializeComponent();

            // Criação do log de eventos
            if (!EventLog.SourceExists("EventSourceDriverIfmO2Ix"))
            {
                EventLog.CreateEventSource("EventSourceDriverIfmO2Ix", "LogDriverO2Ix");
            }
            eventLog = new EventLog()
            {
                Source = "EventSourceDriverIfmO2Ix",
                Log = "LogDriverO2Ix"
            };
        }

        protected override void OnStart(string[] args)
        {
            try
            {
                LerConfiguracoes();
                ConectarLeitor();
                AbrirServidorModbus();
                new Task(Atualizar).Start();
            }
            catch (Exception ex)
            {
                Log(ex.Message);
            }
        }

        private void LerConfiguracoes()
        {
            ipLeitor = IPAddress.Parse(ConfigurationManager.AppSettings["IPLeitor"]);
            portaLeitor = Convert.ToInt32(ConfigurationManager.AppSettings["PortaLeitor"]);
            Log("Configurado IP " + ipLeitor);
            Log("Configurada porta " + portaLeitor);
        }

        private void Atualizar()
        {
            do
            {
                if (clientLeitor == null) { ConectarLeitor(); }
                if (!modbusServerAberto)
                {
                    AbrirServidorModbus();
                }
                else if (!clientLeitor.Connected)
                {
                    ConectarLeitor();
                }
                else
                {
                    try
                    {
                        NetworkStream stream = clientLeitor.GetStream();
                        AtualizarRegistrosCodigo(LerCodigo(stream));
                    }
                    catch (Exception ex)
                    {
                        Log(ex.Message);
                    }
                }
                Thread.Sleep(500);
            } while (!pararServico);
            DesconectarLeitor();
            FecharServidorModbus();
        }

        /// <summary>
        /// Conecta com o leitor de QR Code IFM.
        /// </summary>
        private void ConectarLeitor()
        {
            try
            {
                clientLeitor = new TcpClient();
                clientLeitor.Connect(ipLeitor, portaLeitor);
                Log("Leitor conectado");
            }
            catch (Exception ex)
            {
                Log(ex.Message);
            }
        }

        /// <summary>
        /// Lê o QR Code do leitor conectado.
        /// </summary>
        /// <param name="stream">Stream para o cliente TCP conectado.</param>
        /// <returns>Código QR lido. Retornará uma string vazia se não houver nenhum QR.</returns>
        private string LerCodigo(NetworkStream stream)
        {
            byte[] bufferLeitura = new byte[64];
            byte[] bufferEscrita = Encoding.ASCII.GetBytes("t\n"); // Cria um buffer de escrita com o request: t + LF
            stream.WriteTimeout = 3000;
            stream.ReadTimeout = 3000;
            stream.Write(bufferEscrita, 0, bufferEscrita.Length);
            try
            {
                stream.Read(bufferLeitura, 0, bufferLeitura.Length);
            }
            catch (System.IO.IOException) { } // Ingora erro de timeout

            string codigoLido = Encoding.ASCII.GetString(bufferLeitura).
                Replace("start", string.Empty).Replace("stop", string.Empty); // Remove do código as strings "start" e "stop"
            if (!codigoLido.Contains("fail")) // Se não houve falha de leitura
            {
                return codigoLido; // Retorna o QR code lido
            }
            else // se houve falha de leitura
            {
                return string.Empty; // Retorna uma string vazia
            }
        }

        /// <summary>
        /// Fecha a conexão com o leitor de QR Code.
        /// </summary>
        private void DesconectarLeitor()
        {
            try
            {
                clientLeitor.Close();
            }
            catch { }
            Log("Leitor desconectado.");
        }

        /// <summary>
        /// Inicializa o servidor Modbus para que o Elipse E3 colete os dados.
        /// </summary>
        private void AbrirServidorModbus()
        {
            try
            {
                LiberarFirewall();
                Datastore datastore = new Datastore(1);
                modbusServer = new ModbusSlaveTCP(new Datastore[] { datastore }, IPAddress.Loopback, 502);
                modbusServer.StartListen();
                Log("Servidor Modbus aberto.");
                modbusServerAberto = true;
            }
            catch (Exception ex)
            {
                modbusServerAberto = false;
                Log(ex.Message);
            }
        }

        /// <summary>
        /// Atualiza os holding registers do servidor Modbus com os dados do QR Code lido.
        /// </summary>
        /// <param name="codigo">String do QR Code lido.</param>
        private void AtualizarRegistrosCodigo(string codigo)
        {
            ushort[] registros = new ushort[3];
            if (!string.IsNullOrEmpty(codigo)) // Se foi lido algum QR Code
            {
                byte[] codigoBytes;
                codigoBytes = Encoding.ASCII.GetBytes(codigo);
                for (int i = 0; i < registros.Length; i++) // Junta os 6 bytes em 3 words Modbus
                {
                    registros[i] = (ushort)((codigoBytes[2 * i] << 8) + codigo[2 * i + 1]);
                }
            }
            try
            {
                /* Escreve as words nos holding registers do servidor. Se não tiver sido lido nenhum
                 * QR code, o valor zero será escrito nos três registros */
                modbusServer.ModbusDB[0].HoldingRegisters = registros;
            }
            catch
            {
                FecharServidorModbus();
                throw;
            }
        }

        /// <summary>
        /// Encerra o servidor Modbus TCP.
        /// </summary>
        private void FecharServidorModbus()
        {
            try
            {
                modbusServer.StopListen();
            }
            catch { }
            modbusServerAberto = false;
            Log("Servidor Modbus fechado.");
        }

        protected override void OnStop()
        {
            try
            {
                pararServico = true;
            }
            catch (Exception ex)
            {
                Log(ex.Message);
            }
        }

        /* Método que inicializa e finaliza o serviço, executando-o através de um aplicativo
         * de console. */
        internal void TestStartupAndStop()
        {
            OnStart(null);
            Console.ReadLine();
            OnStop();
        }

        /// <summary>
        /// Registra uma mensagem, dependendo do ambiente em que a aplicação estiver rodando
        /// </summary>
        /// <param name="mensagem"></param>
        private void Log(string mensagem)
        {
            if (Environment.UserInteractive) // Se for uma aplicação de console
            {
                Console.WriteLine(mensagem);
            }
            else
            {
                eventLog.WriteEntry(mensagem);
            }
        }

        /// <summary>
        /// Verifica se uma regra para permissão de entrada de dados na porta TCP 502 já existe e, se não houver, cria-a.
        /// </summary>
        private void LiberarFirewall()
        {
            bool regraExiste = false;
            INetFwPolicy2 fwPolicy = (INetFwPolicy2)Activator.CreateInstance(Type.GetTypeFromProgID("HNetCfg.FWPolicy2"));
            foreach (INetFwRule rule in fwPolicy.Rules)
            {
                if (rule.Name == "Modbus TCP/IP In")
                {
                    regraExiste = true;
                    break;
                }
            }
            if (!regraExiste)
            {
                INetFwRule fwRule = (INetFwRule)Activator.CreateInstance(Type.GetTypeFromProgID("HNetCfg.FWRule"));
                fwRule.Action = NET_FW_ACTION_.NET_FW_ACTION_ALLOW;
                fwRule.Name = "Modbus TCP/IP In";
                fwRule.Description = "Entrada de dados via Modbus TCP/IP";
                fwRule.Direction = NET_FW_RULE_DIRECTION_.NET_FW_RULE_DIR_IN;
                fwRule.Protocol = 6; // 6 para TCP, segundo site http://iana.org
                fwRule.LocalPorts = "502";
                fwRule.Enabled = true;
                fwRule.InterfaceTypes = "All";

                fwPolicy.Rules.Add(fwRule);
                Log("Regra de firewall criada para a porta TCP 502.");
            }
        }
    }
}
