﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace Auttom.OC2018.DriverIFMO2I
{
    static class Program
    {
        /// <summary>
        /// Ponto de entrada principal para o aplicativo.
        /// </summary>
        static void Main()
        {
            if (Environment.UserInteractive) // Se estiver executando como aplicativo de console
            {
                Service1 service1 = new Service1();
                service1.TestStartupAndStop();
            }
            else // Se estiver executando como um serviço
            {
                ServiceBase[] ServicesToRun;
                ServicesToRun = new ServiceBase[]
                {
                new Service1()
                };
                ServiceBase.Run(ServicesToRun);
            }
        }
    }
}
